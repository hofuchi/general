Graham Scanlon
==============

<p style="text-align:left;">
506 Huntercreek Ct.
<span style="float:right;"> gscanlon21@gmail.com</span><br>63131 Des Peres, MO<span style="float:right;">(314)783-7513</span>
</p>

- - -

Technical Experience
--------------------

**Side Projects**
*   [Reverse Dictionary Mobile App](https://bitbucket.org/hofuchi/reversedictionary/) - **react-native**
*   [Morse Code App](https://bitbucket.org/hofuchi/sideprojects/src/ce8778114d146504586a19b2fff05349ba64f020/MorseCodeApp/?at=master) - **C#**
*   [Radio Keyword](https://bitbucket.org/hofuchi/radiokeyword/) (Listens for a keyword from a radio-stream and texts it in for a potential prize) - **Python**
*   [Wallpaper Weather](https://bitbucket.org/hofuchi/sideprojects/src/ce8778114d146504586a19b2fff05349ba64f020/WallpaperWeather/?at=master) (Changes the desktop wallpaper based on the current weather) - **C#**
*   [GUI Pong with Reinforcement Learning](https://bitbucket.org/hofuchi/sideprojects/src/ce8778114d146504586a19b2fff05349ba64f020/pong/?at=master) - **Python**
*   Solutions to several [interview questions](https://bitbucket.org/hofuchi/general-programs/src) - **Python**
*   An all-encompassing database website (_Deprecated_) - **Python**

<br>

**Programming Languages**
* Proficient: **react-native**, **C#**, **JavaScript**, **Python**


* Familiar: **html5**, **css**, **bash**, **git**, **JSON**, **regex**, **MySQL**, **webpack**


<div style="float: left; width: 5%;">
<p></p>
</div>

<div style="float: left; width: 35%;">
<ul>
<h4><u>APIs Used:</u></h4>
<li><a href="https://www.aerisweather.com/">Aeris Weather</a></li>
<li><a href="https://wordnik.com">Wordnik</a></li>
<li><a href="https://datamuse.com">Datamuse</a></li>
<li><a href="https://praw.readthedocs.io/en/latest/">Praw</a></li>
<li><a href="https://foursquare.com">Foursquare</a></li>
<li><a href="https://yelp.com">Yelp</a></li>
<li><a href="https://anagramica.com">Anagramica</a></li>
<li><a href="https://twilio.com">Twilio</a></li>
<li><a href="https://omdbapi.com">OMDb API</a></li>
<li><a href="https://contentful.com/">Contentful</a></li>
</ul>
</div>

<div style="float: right; width: 60%;">
<ul>
<h4><u>Frameworks/Libraries Used:</u></h4>
<li><a href="https://www.xamarin.com/">Xamarin</a> - For developing native apps using C#</li>
<li><a href="https://github.com/airbnb/enzyme">Enzyme</a> - Unit testing</li>
<li><a href="https://facebook.github.io/jest">Jest</a> - Unit testing</li>
<li><a href="https://docs.python.org/3/library/unittest.html">unittest</a> - Unit testing</li>
<li><a href="https://www.pythonanywhere.com">pythonanywhere</a> - Website hosting</li>
<li><a href="http://flask.pocoo.org">flask</a> - For web development</li>
<li><a href="https://pypi.python.org/pypi/bcrypt/3.1.0">bcrypt</a> - AES, Hashing, Salting</li>
<li><a href="https://docs.python.org/3/library/multiprocessing.html"> multiprocessing</a> - Threading, Processes, Locks</li>
<li><a href="https://www.sqlalchemy.org">sqlalchemy</a> - Databases</li>
<li><a href="https://pypi.python.org/pypi/PyQt5">PyQt5</a> - UI and application toolkit</li>
</ul>
</div>

&nbsp;

Education
---------

<div style="float: left; width: 25%;">
2016-2017 (ongoing)
</div>
<div style="float: right; width: 75%;">
<strong>Software Development</strong>; Self-taught
</div>
<div style="float: right; width: 75%;">
<a href="https://bitbucket.org/hofuchi/">Bitbucket Profile - bitbucket.org/hofuchi/</a>
</div>

&nbsp;

<div style="float: left; width: 25%;">
2017-2017 (expected)
</div>
<div style="float: right; width: 75%;">
<strong>Software Development</strong>; LaunchCode's lc101 course
</div>

<br>
<br>

<div style="float: left; width: 25%;">
2017-2017
</div>
<div style="float: right; width: 75%;">
<strong>General Education</strong>; St. Louis Community college (Meramec)
</div>
<div style="float: right; width: 75%;">
<em>One semester of pre-calc</em>
</div>
