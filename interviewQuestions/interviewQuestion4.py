#! usr/bin/python3
# -*- coding: utf-8 -*-
import unittest

"""
Two non-negative integers A and B are given.

For example, 53 occurs in 1953786 at position 2.
78 occurs in 195378678 at positions 4 and 7.
57 does not occur in 153786.

Write a function that, given two non-negative integers A and B, returns
the leftmost position at which A occurs in B.

The function should return −1 if A does not occur in B.
"""


class Solution:
    def __init__(self):
        pass

    def numinNum(self, num, subNum):
        num, subNum = str(num), str(subNum)
        try:
            # Gets the leftmost position of the subNum in num
            return num.index(subNum)
        except ValueError:
            # subNum not in num
            return -1


class MyTest(unittest.TestCase):
    def test_me(self):
        S = Solution()
        aE = self.assertEqual
        aE(S.numinNum(9919919, 19), 2)  # OK
        aE(S.numinNum(99199, 99), 0)  # OK
        aE(S.numinNum(999999, 19), -1)  # OK


if __name__ == '__main__':
    unittest.main()
