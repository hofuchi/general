#! python3
# -*- coding: utf-8 -*-
import unittest

# Given two lists and a number:
# Find all matching pairs of between the two lists that sum to the number


class ListSolver:
    def __init__(self):
        pass

    def pairsThatAddToANumber(self, array1, array2, S):
        returnlist = []
        for ite in array1:
            for item in array2:
                if ite + item == S:
                    success = [ite, item]
                    returnlist.append(success)

        return (returnlist)


class MyTest(unittest.TestCase):
    def test_me(self):
        LS = ListSolver()
        aE = self.assertEqual
        aE(LS.pairsThatAddToANumber([5, 7, 10, -3, 8], [2, -4, -3, 0], 7), ([[5, 2], [7, 0], [10, -3]]))  # OK


unittest.main()
